package com.dev.merapitech.merapiattendance.utils

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.dev.merapitech.merapiattendance.R
import com.dev.merapitech.merapiattendance.model.HistoryModel
import com.dev.merapitech.merapiattendance.model.TaskModel
import com.dev.merapitech.merapiattendance.utils.Session
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Text

object Handle {

    lateinit var session: Session

//    fun handleUserAll(sjson: String): Boolean {
//        try {
//
//            val jsonObject = JSONObject(sjson)
//
//
//            val data = jsonObject.getJSONArray("users")
//            if (data.length() >= 0) {
//                Log.d("data123", Integer.toString(data.length()))
//                for (i in 0 until data.length()) {
////                    val user = User()
////                    user.setName(data.getJSONObject(i).getString("name"))
////                    user.setUsername(data.getJSONObject(i).getString("username"))
////                    Log.d("name123", data.getJSONObject(i).getString("name"))
////                    Api.userResponses.add(user)
//                }
//                return true
//            } else {
//                Log.d("trip", "data not found")
//            }
//            return true
//        } catch (e: JSONException) {
//
//        } catch (e: Exception) {
//
//        }
//
//        return false
//    }

    fun handleLogin(sjson: String, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
            if (status == "200") {
                val data = jsonObject.getJSONObject("data")
                session.save("apikey", data.getString("apikey"))
                session.save("email", data.getString("email"))
                session.save("full_name", data.getString("full_name"))
                session.save("unit_name", data.getString("unit_name"))
                session.save("no_telp", data.getString("no_telp"))
                return true
            } else {
                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

    fun handleCheckApiKey(sjson: String, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
            if (status == "200") {

                return true
            } else {

                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

    fun handleStatusAbsen(sjson: String, tvCheckIn: TextView, tvCheckOut: TextView, btnCheckIn: AppCompatButton, btnCheckOut: AppCompatButton, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
            val data = jsonObject.getJSONObject("data")
            tvCheckIn.text = data.getString("checkin_time")
            tvCheckOut.text = data.getString("checkout_time")
            if (status == "200") {
                if (data.getString("checkin") == "0"){
                    btnCheckOut.isEnabled = false
                    btnCheckOut.setBackgroundDrawable(context.resources.getDrawable(R.drawable.button_check_inactive))

                    btnCheckIn.isEnabled = true
                    btnCheckIn.setBackgroundDrawable(context.resources.getDrawable(R.drawable.button_check_active))
                } else if (data.getString("checkin") == "1" && data.getString("checkout") == "1"){
                    btnCheckOut.isEnabled = false
                    btnCheckOut.setBackgroundDrawable(context.resources.getDrawable(R.drawable.button_check_inactive))

                    btnCheckIn.isEnabled = false
                    btnCheckIn.setBackgroundDrawable(context.resources.getDrawable(R.drawable.button_check_inactive))
                } else {
                    btnCheckIn.isEnabled = false
                    btnCheckIn.setBackgroundDrawable(context.resources.getDrawable(R.drawable.button_check_inactive))

                    btnCheckOut.isEnabled = true
                    btnCheckOut.setBackgroundDrawable(context.resources.getDrawable(R.drawable.button_check_active))
                }
                return true
            } else {

                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

    fun handleAbsen(sjson: String, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
//            val data = jsonObject.getJSONObject("data")
            if (status == "200") {

                return true
            } else {

                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

    fun handleHistory(sjson: String, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
            if (status == "200") {
                Api.historyList.clear()
                val data = jsonObject.getJSONArray("data")
                if (data.length() >= 0) {
                    for (i in 0 until data.length()) {
                        val history = HistoryModel()
                        history.date = data.getJSONObject(i).getString("time")
                        history.status = data.getJSONObject(i).getString("type")
                        Api.historyList.add(history)
                    }
                }
                return true
            } else {

                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

    fun handleTask(sjson: String, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
            if (status == "200") {
                Api.taskList.clear()
                val data = jsonObject.getJSONArray("data")
                if (data.length() >= 0) {
                    for (i in 0 until data.length()) {
                        val task = TaskModel()
                        task.date = data.getJSONObject(i).getString("date")
                        task.title = data.getJSONObject(i).getString("title")
                        task.project_name = data.getJSONObject(i).getString("project_name")
                        task.id = data.getJSONObject(i).getString("id")
                        task.id_epic = data.getJSONObject(i).getString("id_epic")
                        task.epic_name = data.getJSONObject(i).getString("epic_name")

                        Api.taskList.add(task)
                    }
                }
                return true
            } else {

                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

    fun handleTaskFinish(sjson: String, context: Context): Boolean {

        val session = Session(context)
        try {
            val jsonObject = JSONObject(sjson)
            val status = jsonObject.getString("status")
            if (status == "200") {
                Api.taskListFinish.clear()
                val data = jsonObject.getJSONArray("data")
                if (data.length() >= 0) {
                    for (i in 0 until data.length()) {
                        val task = TaskModel()
                        task.date = data.getJSONObject(i).getString("date")
                        task.title = data.getJSONObject(i).getString("title")
                        task.project_name = data.getJSONObject(i).getString("project_name")
                        task.id = data.getJSONObject(i).getString("id")
                        task.id_epic = data.getJSONObject(i).getString("id_epic")
                        task.epic_name = data.getJSONObject(i).getString("epic_name")

                        Api.taskListFinish.add(task)
                    }
                }
                return true
            } else {

                return false

            }

        } catch (e: JSONException) {

        } catch (e: java.lang.Exception) {

        }

        return false
    }

//
//    fun handleLoginSQL(sjson: String, password: String, context: Context): Boolean {
//        val session = Session(context)
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (jsonObject.getString("success").equals("1")) {
//                session.save("password", password)
//                return true
//            }else{
//                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
//                return false
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }
//
//    fun handleRegister(sjson: String, context: Context): Boolean {
//        try {
//            val jsonObject = JSONObject(sjson)
//
//           return true
//        } catch (e: JSONException) {
//            return false
//        } catch (e: java.lang.Exception) {
//            return false
//        }
//
//    }
//
//    fun handleUpdateEmail(sjson: String, context: Context): Boolean {
//
//        val session = Session(context)
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (!TextUtils.isEmpty(jsonObject.getString("username"))) {
//                return true
//
//            } else {
//                val intent = Intent(context, ProfileActivity::class.java)
//                context.startActivity(intent)
//                return false
//
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }
//
//    fun handleUpdateName(sjson: String, context: Context): Boolean {
//
//        val session = Session(context)
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (!TextUtils.isEmpty(jsonObject.getString("username"))) {
//
//                return true
//
//            } else {
//                val intent = Intent(context, ProfileActivity::class.java)
//                context.startActivity(intent)
//                return false
//
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }
//
//    fun handleRegisterSQL(sjson: String, context: Context): Boolean {
//        try {
//            val jsonObject = JSONObject(sjson)
//            if (jsonObject.getString("success").equals("1")) {
//                return true
//            }else{
//                return false
//            }
//        } catch (e: JSONException) {
//            return false
//        } catch (e: java.lang.Exception) {
//            return false
//        }
//    }
//
//    fun handleUpdateEmailSQL(sjson: String, context: Context): Boolean {
//
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (jsonObject.getString("success").equals("1")) {
//                return true
//            }else{
//                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
//                return false
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }
//
//    fun handleUpdateNameSQL(sjson: String, context: Context): Boolean {
//
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (jsonObject.getString("success").equals("1")) {
//                return true
//            }else{
//                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
//                return false
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }
//
//    fun handleInviteFriendSQL(sjson: String, context: Context): Boolean {
//
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (jsonObject.getString("success").equals("1")) {
//                return true
//            }else{
//                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
//                return false
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }
//
//    fun handleAcceptFriendSQL(sjson: String, context: Context): Boolean {
//
//        try {
//            val jsonObject = JSONObject(sjson)
//
//            if (jsonObject.getString("success").equals("1")) {
//                return true
//            }else{
//                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
//                return false
//            }
//
//        } catch (e: JSONException) {
//
//        } catch (e: java.lang.Exception) {
//
//        }
//
//        return false
//    }




    //    private Session session;
    //
    //    public static boolean handleLogin(String sjson, Context context) {
    //
    //        Session session = new Session(context);
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (!TextUtils.isEmpty(jsonObject.getJSONObject("data").getString("token"))) {
    //
    //                session.save("token", jsonObject.getJSONObject("data").getString("token"));
    //                session.save("id", jsonObject.getJSONObject("data").getString("id"));
    //                session.save("name", jsonObject.getJSONObject("data").getString("name"));
    //                session.save("email", jsonObject.getJSONObject("data").getString("email"));
    //                session.save("regions_id", jsonObject.getJSONObject("data").getString("regions_id"));
    //                session.save("regencies_id", jsonObject.getJSONObject("data").getString("regencies_id"));
    //                session.save("districts_id", jsonObject.getJSONObject("data").getString("districts_id"));
    //                session.save("postcode", jsonObject.getJSONObject("data").getString("postcode"));
    //                session.save("address", jsonObject.getJSONObject("data").getString("address"));
    //
    //                Intent intent = new Intent(context, HomeActivity.class);
    //                context.startActivity(intent);
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleGetProvinsi(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        ProvinsiModel provinsi = new ProvinsiModel();
    //                        provinsi.setId(data.getJSONObject(i).getString("id"));
    //                        provinsi.setCountry_id(data.getJSONObject(i).getString("country_id"));
    //                        provinsi.setName(data.getJSONObject(i).getString("name"));
    //                        Api.provinsiModels.add(provinsi);
    //                    }
    //                    return true;
    //                } else {
    //                    Log.d("trip", "data not found");
    //                }
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleGetProvinsiName(String sjson, TextInputEditText etProvinsi, Context context) {
    //        Session session = new Session(context);
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        if (data.getJSONObject(i).getString("id").equals(session.get("regions_id"))){
    //                            etProvinsi.setText(data.getJSONObject(i).getString("name"));
    //                            UbahAlamatActivity.idProv = data.getJSONObject(i).getString("id");
    //                            return true;
    //                        }
    //                    }
    //                } else {
    //                    Log.d("trip", "data not found");
    //                }
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleGetKabupaten(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        KabupatenModel kabupaten = new KabupatenModel();
    //                        kabupaten.setId(data.getJSONObject(i).getString("id"));
    //                        kabupaten.setRegion_id(data.getJSONObject(i).getString("region_id"));
    //                        kabupaten.setName(data.getJSONObject(i).getString("name"));
    //                        kabupaten.setCreated_at(data.getJSONObject(i).getString("created_at"));
    //                        kabupaten.setUpdated_at(data.getJSONObject(i).getString("updated_at"));
    //
    //                        Api.kabupatenModels.add(kabupaten);
    //                    }
    //                    return true;
    //                } else {
    //                    Log.d("trip", "data not found");
    //                }
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleGetKabupatenName(String sjson, TextInputEditText etKabupaten, Context context) {
    //        Session session = new Session(context);
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        if (data.getJSONObject(i).getString("id").equals(session.get("regencies_id"))){
    //                            etKabupaten.setText(data.getJSONObject(i).getString("name"));
    //                            UbahAlamatActivity.idKab = data.getJSONObject(i).getString("id");
    //                            return true;
    //                        }
    //                    }
    //                } else {
    //                    Log.d("trip", "data not found");
    //                }
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleGetKecamatan(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        KecamatanModel kecamatan = new KecamatanModel();
    //                        kecamatan.setId(data.getJSONObject(i).getString("id"));
    //                        kecamatan.setRegency_id(data.getJSONObject(i).getString("regency_id"));
    //                        kecamatan.setName(data.getJSONObject(i).getString("name"));
    //                        kecamatan.setCreated_at(data.getJSONObject(i).getString("created_at"));
    //                        kecamatan.setUpdated_at(data.getJSONObject(i).getString("updated_at"));
    //
    //                        Api.kecamatanModels.add(kecamatan);
    //                    }
    //                    return true;
    //                } else {
    //
    //                }
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleGetKecamatanName(String sjson, TextInputEditText etKecamatan, Context context) {
    //        Session session = new Session(context);
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        if (data.getJSONObject(i).getString("id").equals(session.get("districts_id"))){
    //                            etKecamatan.setText(data.getJSONObject(i).getString("name"));
    //                            UbahAlamatActivity.idKec = data.getJSONObject(i).getString("id");
    //                            Log.d("Kecamatan name", data.getJSONObject(i).getString("name"));
    //                            return true;
    //                        }
    //                    }
    //                } else {
    //                    Log.d("trip", "data not found");
    //                }
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleRequestRegister(String sjson, Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //
    //                return true;
    //
    //            } else {
    //                Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleChangePhone(String sjson, Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //
    //                return true;
    //
    //            } else {
    //                Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleChangeEmail(String sjson, Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            boolean succses = jsonObject.getBoolean("success");
    //            if (succses) {
    //
    //                return true;
    //
    //            } else {
    //                Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleChangeAddressDetail(String sjson, TextInputEditText etKodepos, TextInputEditText etAddress, Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            etKodepos.setText(jsonObject.getJSONObject("data").getString("postcode"));
    //            etAddress.setText(jsonObject.getJSONObject("data").getString("address"));
    //
    //            return true;
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleHome(String sjson, TextView tvName, TextView tvEmail, Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            tvName.setText(jsonObject.getJSONObject("data").getString("name"));
    //            tvEmail.setText(jsonObject.getJSONObject("data").getString("email"));
    //
    //            return true;
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //

    //
    //    public static boolean handleProfileDetail(String sjson, TextView tvName, TextInputEditText etName,
    //                                              TextInputEditText etId, TextInputEditText etPhone,
    //                                              TextInputEditText etEmail, TextInputEditText etAddress,
    //                                              Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            tvName.setText(jsonObject.getJSONObject("data").getString("name"));
    //            etName.setText(jsonObject.getJSONObject("data").getString("name"));
    //            etId.setText(jsonObject.getJSONObject("data").getString("id"));
    //            etPhone.setText(jsonObject.getJSONObject("data").getString("phone"));
    //            etEmail.setText(jsonObject.getJSONObject("data").getString("email"));
    //            etAddress.setText(jsonObject.getJSONObject("data").getString("address"));
    //
    //            return true;
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleProfilePengaturan(String sjson, TextView tvName, ConstraintLayout btnChangePhone,
    //                                                  ConstraintLayout btnChangeEmail, ConstraintLayout btnChangeBank,
    //                                                  ConstraintLayout btnChangeAddress, ConstraintLayout btnChangePassword,
    //                                                  Context context) {
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            tvName.setText(jsonObject.getJSONObject("data").getString("name"));
    //
    //            btnChangePhone.setOnClickListener(new View.OnClickListener() {
    //                @Override
    //                public void onClick(View view) {
    //                    Intent intent = new Intent(context, UbahNomorActivity.class);
    //                    try {
    //                        intent.putExtra("PHONE", jsonObject.getJSONObject("data").getString("phone"));
    //                    } catch (JSONException e) {
    //                        e.printStackTrace();
    //                    }
    //                    context.startActivity(intent);
    //                }
    //            });
    //
    //            btnChangeEmail.setOnClickListener(new View.OnClickListener() {
    //                @Override
    //                public void onClick(View view) {
    //                    Intent intent = new Intent(context, UbahEmailActivity.class);
    //                    try {
    //                        intent.putExtra("EMAIL", jsonObject.getJSONObject("data").getString("email"));
    //                    } catch (JSONException e) {
    //                        e.printStackTrace();
    //                    }
    //                    context.startActivity(intent);
    //                }
    //            });
    //
    //            btnChangeAddress.setOnClickListener(new View.OnClickListener() {
    //                @Override
    //                public void onClick(View view) {
    //                    Intent intent = new Intent(context, UbahAlamatActivity.class);
    //                    context.startActivity(intent);
    //                }
    //            });
    //
    //            btnChangePassword.setOnClickListener(new View.OnClickListener() {
    //                @Override
    //                public void onClick(View view) {
    //                    Intent intent = new Intent(context, UbahPasswordActivity.class);
    //                    context.startActivity(intent);
    //                }
    //            });
    //
    //            return true;
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //    public static boolean handleLogin(String sjson, Context context) {
    //
    //        Session session = new Session(context);
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //                session.save(context.getString(R.string.apikey), jsonObject.getJSONObject("data").getString(context.getString(R.string.apikey)).toString());
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleProd(String sjson, Context context, String code){
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            int succses = Integer.parseInt(jsonObject.getString("status").toString());
    //            if (succses == 200) {
    //                JSONArray data = new JSONArray();
    //                data = jsonObject.getJSONArray("data");
    //
    //                if (data.length() >= 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        ModelProduct mprod = new ModelProduct();
    //                        mprod.setHeader(code);
    //                        mprod.setIdProd(data.getJSONObject(i).getString("id_product"));
    //                        mprod.setItem(data.getJSONObject(i).getString("nama"));
    //                        mprod.setDesc(data.getJSONObject(i).getString("description"));
    //                        mprod.setPrice(data.getJSONObject(i).getString("harga"));
    //                        mprod.setDisc(data.getJSONObject(i).getString("discount"));
    //                        mprod.setCrystal(data.getJSONObject(i).getString("crystal"));
    //                        mprod.setType(data.getJSONObject(i).getString("type"));
    //                        mprod.setDetail(data.getJSONObject(i).getString("detail"));
    //
    //                        Api.prodList.add(mprod);
    //                    }
    //                    return true;
    //
    //                } else {
    //
    //                    return false;
    //
    //                }
    //
    //            } else {
    //                return false;
    //            }
    //
    //        } catch (JSONException e) {
    //            e.printStackTrace();
    //        }catch (Exception e){
    //            e.printStackTrace();
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleHome(String sjson, Context context, View view) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //
    //
    //                JSONObject json = new JSONObject(jsonObject.getJSONObject("data")+"");
    //                Helper.setText(R.id.txt_tgl, view, json.getString("tanggal"));
    //                Helper.setText(R.id.txt_usrtotal,view,Helper.getNumberFormat(Integer.parseInt(json.getString("user_all"))));
    //                Helper.setText(R.id.txt_usronline,view,Helper.getNumberFormat(Integer.parseInt(json.getString("user_online"))));
    //                Helper.setText(R.id.txt_usronlinepercent,view,json.getString("user_online_persentase") + "%");
    //                Helper.setText(R.id.txt_usroffline,view, Helper.getNumberFormat(Integer.parseInt(json.getString("user_ofline"))));
    //                Helper.setText(R.id.txt_omseth1,view,Helper.getNumberFormatCurrency(Integer.parseInt(json.getString("omset_bulan_lalu"))));
    //                Helper.setText(R.id.txt_cashout,view,Helper.getNumberFormatCurrency(Integer.parseInt(json.getString("pengeluaran_bulan_lalu"))));
    //                Helper.setText(R.id.txt_netto,view,Helper.getNumberFormatCurrency(Integer.parseInt(json.getString("omset_bulan_lalu"))- Integer.parseInt(json.getString("pengeluaran_bulan_lalu"))));
    //                Helper.setText(R.id.txt_registration, view, Helper.getNumberFormat(Integer.parseInt(json.getString("user_registrasi_week"))));
    //                Helper.setText(R.id.txt_active, view, Helper.getNumberFormat(Integer.parseInt(json.getString("user_active_week"))));
    //
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleDoUn(String sjson, Context context, View view) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //                JSONObject json_data = new JSONObject(jsonObject.getJSONObject("data")+"");
    //                JSONObject json_day = new JSONObject(json_data.getJSONObject("day")+"");
    //                JSONObject json_month = new JSONObject(json_data.getJSONObject("month")+"");
    //                JSONObject json_year = new JSONObject(json_data.getJSONObject("year")+"");
    //
    //                //install
    //                Helper.setText(R.id.txt_todown,view, Helper.getNumberFormat(Integer.parseInt(json_day.getString("install"))));
    //                Helper.setText(R.id.txt_modown,view, Helper.getNumberFormat(Integer.parseInt(json_month.getString("install"))));
    //                Helper.setText(R.id.txt_yedown,view, Helper.getNumberFormat(Integer.parseInt(json_year.getString("install"))));
    //
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleTransaksi(String sjson, Context context, View view) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //                JSONObject json_data = new JSONObject(jsonObject.getJSONObject("data")+"");
    //
    //                Helper.setText(R.id.txt_trxnow,view, Helper.getNumberFormatCurrency(Integer.parseInt(json_data.getString("day"))));
    //                Helper.setText(R.id.txt_trxmonth,view, Helper.getNumberFormatCurrency(Integer.parseInt(json_data.getString("month"))));
    //                Helper.setText(R.id.txt_trxyear,view, Helper.getNumberFormatCurrency(Integer.parseInt(json_data.getString("year"))));
    //
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() > 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        ModelTransaksi transaksi = new ModelTransaksi();
    //                        transaksi.setNama(data.getJSONObject(i).getString("nama"));
    //                        transaksi.setQty(data.getJSONObject(i).getString("qty"));
    //                        transaksi.setHarga(data.getJSONObject(i).getString("harga"));
    //                        transaksi.setTotal(data.getJSONObject(i).getString("total"));
    //                        Api.transList.add(transaksi);
    //
    //                    }
    //                    return true;
    //
    //                } else {
    //
    //                    return false;
    //                }
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //
    //    public static boolean handleTransaksiDetail(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() > 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        ModelTransaksi transaksi = new ModelTransaksi();
    //                        transaksi.setNama(data.getJSONObject(i).getString("nama"));
    //                        transaksi.setQty(data.getJSONObject(i).getString("qty"));
    //                        transaksi.setHarga(data.getJSONObject(i).getString("harga"));
    //                        transaksi.setTotal(data.getJSONObject(i).getString("total"));
    //                        Api.transList.add(transaksi);
    //
    //                    }
    //                    return true;
    //
    //                } else {
    //
    //                    return false;
    //                }
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleUserActive(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() > 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        ModelUser user = new ModelUser();
    //                        user.setEmail(data.getJSONObject(i).getString("email"));
    //                        user.setRegistrasi(data.getJSONObject(i).getString("registrasi"));
    //                        user.setLast_login(data.getJSONObject(i).getString("last_login"));
    //
    //                        Api.userList.add(user);
    //
    //                    }
    //                    return true;
    //
    //                } else {
    //
    //                    return false;
    //                }
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
    //
    //    public static boolean handleAnggaran(String sjson, Context context){
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //                JSONArray data = jsonObject.getJSONArray("data");
    //                if (data.length() > 0) {
    //                    for (int i = 0; i < data.length(); i++) {
    //                        ModelAnggaran manggaran = new ModelAnggaran();
    //                        manggaran.setIdrab(data.getJSONObject(i).getString("id_rab"));
    //                        manggaran.setRabname(data.getJSONObject(i).getString("rab_name"));
    //                        manggaran.setCreatedat(data.getJSONObject(i).getString("created_at"));
    //                        manggaran.setNominal(data.getJSONObject(i).getString("nominal"));
    //
    //                       Api.anggaranList.add(manggaran);
    //
    //                    }
    //                    return true;
    //
    //                } else {
    //
    //                    return false;
    //
    //                }
    //
    //            } else {
    //                return false;
    //            }
    //
    //        } catch (JSONException e) {
    //            e.printStackTrace();
    //        }catch (Exception e){
    //            e.printStackTrace();
    //        }
    //
    //        return false;
    //    }
    //
    //    public static String handleApproveRAB(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //
    //                return jsonObject.getString(context.getString(R.string.status));
    //
    //            } else {
    //
    //                return jsonObject.getString(context.getString(R.string.status));
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return "Gagal, Silahkan coba lagi";
    //    }
    //
    //    public static boolean handleApprovePlay(String sjson, Context context) {
    //
    //        try {
    //            JSONObject jsonObject = new JSONObject(sjson);
    //
    //            if (jsonObject.getString(context.getString(R.string.status)).equals("200")) {
    //
    //
    //                return true;
    //
    //            } else {
    //
    //                return false;
    //
    //            }
    //
    //        } catch (JSONException e) {
    //
    //        } catch (Exception e) {
    //
    //        }
    //
    //        return false;
    //    }
}
