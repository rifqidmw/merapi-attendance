package com.dev.merapitech.merapiattendance.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.dev.merapitech.merapiattendance.R
import com.dev.merapitech.merapiattendance.model.HistoryModel

class HistoryAdapter(val context: Context, var list: ArrayList<HistoryModel> = arrayListOf(), var tipe: Int?) : RecyclerView.Adapter<HistoryAdapter.HistoryHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return HistoryHolder(view)
    }

    override fun getItemCount(): Int {
        return if (tipe == 1) {
            Math.min(list.size, 5)
        } else {
            list.size
        }
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
        val data = list[position]

        if (data.status == "1"){
            holder.lyStatus.setBackgroundResource(R.drawable.button_success)
            holder.tvDate.text = data.date
            holder.tvStatus.text = "Check In"
        } else {
            holder.lyStatus.setBackgroundResource(R.drawable.button_late)
            holder.tvDate.text = data.date
            holder.tvStatus.text = "Check Out"
        }

    }

    inner class HistoryHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvDate: TextView = itemView.findViewById(R.id.tv_date)
        var tvStatus: TextView = itemView.findViewById(R.id.tv_status)
        var lyStatus: ConstraintLayout = itemView.findViewById(R.id.layout_status)
    }
}

