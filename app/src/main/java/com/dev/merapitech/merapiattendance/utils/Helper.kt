package com.dev.merapitech.merapiattendance.utils

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.*
import com.dev.merapitech.merapiattendance.MainActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.NumberFormat
import java.util.*
import kotlin.experimental.and


@SuppressLint("StaticFieldLeak")
object Helper {


    var mContext: Context? = null

    fun edcgold(): MainActivity {
        return mContext as MainActivity
    }

    fun rootView(): View {
        return edcgold().window.decorView.rootView
    }

    /*
	 * Edit Text Helpers
	 */
    @JvmOverloads
    fun edit(res: Int, view: View = rootView()): EditText {
        return view.findViewById<View>(res) as EditText
    }

    fun getEdtStr(res: Int, view: View): String {
        return getEditString(res, view, false)
    }

    fun getEditString(res: Int, view: View, sanitizeName: Boolean): String {
        val result = edit(res, view).text.toString()

        return if (sanitizeName) sanitizeName(result) else result
    }


    /*
	 * TextView Helpers
	 */

    fun tv(res: Int, view: View): TextView {
        return view.findViewById<View>(res) as TextView
    }


    fun edt(res: Int): TextView {
        return tv(res, rootView())
    }


    fun setText(res: Int, view: View, value: String) {
        tv(res, view).text = value
    }

    fun setText(res: Int, value: String) {
        tv(res, rootView()).text = value
    }

    fun setText(res: Int, view: View, resStr: Int) {
        tv(res, view).setText(resStr)
    }

    fun setText(res: Int, resStr: Int) {
        tv(res, rootView()).setText(resStr)
    }

    /*
	 * Other Helpers
	 */

    @JvmOverloads
    fun img(res: Int, view: View = rootView()): ImageView {
        return view.findViewById<View>(res) as ImageView
    }


    @JvmOverloads
    fun button(res: Int, view: View = rootView()): Button {
        return view.findViewById<View>(res) as Button
    }

    @JvmOverloads
    fun simpleButton(res: Int, view: View = rootView()): TextView {
        return view.findViewById<View>(res) as TextView
    }

    @JvmOverloads
    fun linear(res: Int, view: View = rootView()): LinearLayout {
        return view.findViewById<View>(res) as LinearLayout
    }

    @JvmOverloads
    fun imgBtn(res: Int, view: View = rootView()): ImageButton {
        return view.findViewById<View>(res) as ImageButton
    }

    fun sanitizeName(name: String): String {
        return name.replace("[^a-zA-Z0-9]+".toRegex(), "")
    }

    @Throws(NoSuchAlgorithmException::class)
    fun SHA1(input: String): String {

        val mDigest = MessageDigest.getInstance("SHA1")
        val result = mDigest.digest(input.toByteArray())

        val SHAStr = StringBuffer()
        for (i in result.indices) {
            SHAStr.append(Integer.toString((result[i] and 0xff.toByte()) + 0x100, 16).substring(1))
        }

        return SHAStr.toString()
    }

    fun getNumberFormatCurrency(number: Int): String {
        val numberFormatter = NumberFormat.getNumberInstance(Locale.GERMANY)
        val output = numberFormatter.format(number.toLong())
        return "Rp $output"
    }

    fun getNumberFormatCurrencyDoub(number: Double): String {
        val numberFormatter = NumberFormat.getNumberInstance(Locale.GERMANY)
        val output = numberFormatter.format(number)
        return "Rp $output"
    }

    fun getNumberFormat(number: Int): String {
        val numberFormatter = NumberFormat.getNumberInstance(Locale.GERMANY)
        return numberFormatter.format(number.toLong())
    }


}
