package com.dev.merapitech.merapiattendance

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.dev.merapitech.merapiattendance.utils.Api
import com.dev.merapitech.merapiattendance.utils.Handle
import com.dev.merapitech.merapiattendance.utils.ParamReq
import com.dev.merapitech.merapiattendance.utils.Session
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

class LoginActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session
    private lateinit var dialog: android.app.AlertDialog

    private var imeiDevice: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        session = Session(this@LoginActivity)
        dialog = SpotsDialog.Builder().setContext(this@LoginActivity).build()

        try {
            if (!session.get("apikey").isNullOrEmpty()) {
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            }
        } catch (e: Exception) {

        }

        btn_login.setOnClickListener {
            if (et_email.text.toString().isNotEmpty() && et_password.text.toString().isNotEmpty()){
                onImeiCheckPermission()
            } else {
                Toast.makeText(this@LoginActivity, "Harap email & password diisi!!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun login(){
        dialog.show()
        val call = ParamReq.req1001("1001", et_email.text.toString(), et_password.text.toString(), imeiDevice, "1", this@LoginActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {

                    val handle = Handle.handleLogin(response.body()!!.string(), this@LoginActivity)
                    if (handle) {
                        dialog.dismiss()
                        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    } else {
                        dialog.dismiss()
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@LoginActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@LoginActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@LoginActivity, call, cBack, false, "Loading")
    }

    private fun onImeiCheckPermission(){
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.READ_PHONE_STATE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        val tel = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

                        if (ActivityCompat.checkSelfPermission(this@LoginActivity,
                                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        }
                        val imei = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            tel.imei
                        } else {
                            tel.deviceId
                        }

                        imeiDevice = imei
                        if (imei != ""){
                            login()
                        }
                    } else {
                        showSettingsDialog()
                    }

                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this@LoginActivity)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()

    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }
}
