package com.dev.merapitech.merapiattendance.utils

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

class NotificationUtils {

    fun setNotification(context: Context) {



        val alarmManager = context.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
        val alarmIntent = Intent(context.applicationContext, AlarmReceiver::class.java) // AlarmReceiver1 = broadcast receiver

        alarmIntent.putExtra("reason", "notification")


        val calendar = Calendar.getInstance()


        val pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }
}