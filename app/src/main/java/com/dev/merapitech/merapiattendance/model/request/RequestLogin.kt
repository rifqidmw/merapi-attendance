package com.dev.merapitech.merapiattendance.model.request

class RequestLogin {
    lateinit var HeaderCode: String
    lateinit var signature: String
    lateinit var data: DataLogin
    class DataLogin{
        lateinit var email: String
        lateinit var password: String
        lateinit var id_device: String
        lateinit var jenis_device: String
    }
}