package com.dev.merapitech.merapiattendance

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.merapitech.merapiattendance.adapter.HistoryAdapter
import com.dev.merapitech.merapiattendance.model.HistoryModel
import com.dev.merapitech.merapiattendance.utils.Api
import com.dev.merapitech.merapiattendance.utils.Handle
import com.dev.merapitech.merapiattendance.utils.ParamReq
import com.dev.merapitech.merapiattendance.utils.Session
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_history.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

class HistoryActivity : AppCompatActivity() {

    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var dataList: ArrayList<HistoryModel>
    private lateinit var session: Session
    private lateinit var dialog: android.app.AlertDialog
    private lateinit var cBack: Callback<ResponseBody>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        dialog = SpotsDialog.Builder().setContext(this@HistoryActivity).build()
        session = Session(this@HistoryActivity)

        toolbar.setNavigationOnClickListener {
            val activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this@HistoryActivity, rv_history, "recyclerHistory")

            val intent = Intent(this@HistoryActivity, MainActivity::class.java)
            startActivity(intent, activityOptionsCompat.toBundle())
            this@HistoryActivity.finish()
        }

        Api.historyList = ArrayList<HistoryModel>()
        historyAdapter = HistoryAdapter(this, Api.historyList, 2)

        rv_history.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_history.itemAnimator = DefaultItemAnimator()
        rv_history.adapter = historyAdapter

        getHistory()
    }

    private fun getHistory(){
        dialog.show()
        val call = ParamReq.req2003("2003", session.get("apikey")!!, this@HistoryActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleHistory(response.body()!!.string(), this@HistoryActivity)
                    if (handle) {
                        historyAdapter.notifyDataSetChanged()
                        dialog.dismiss()
                    } else {
                        dialog.dismiss()
                        Toast.makeText(this@HistoryActivity, "Gagal Mengambil History Absen", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@HistoryActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@HistoryActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@HistoryActivity, call, cBack, false, "Loading")
    }

    private fun loadData(){
        dataList.clear()
        val data1 = HistoryModel()
        data1.date = "12 September 2019"
        data1.status = "2"
        dataList.add(data1)

        val data2 = HistoryModel()
        data2.date = "13 September 2019"
        data2.status = "1"
        dataList.add(data2)

        val data3 = HistoryModel()
        data3.date = "14 September 2019"
        data3.status = "2"
        dataList.add(data3)

        val data4 = HistoryModel()
        data4.date = "15 September 2019"
        data4.status = "1"
        dataList.add(data4)

        val data5 = HistoryModel()
        data5.date = "16 September 2019"
        data5.status = "2"
        dataList.add(data5)

        val data6 = HistoryModel()
        data6.date = "17 September 2019"
        data6.status = "2"
        dataList.add(data6)

        val data7 = HistoryModel()
        data7.date = "18 September 2019"
        data7.status = "2"
        dataList.add(data7)

        val data8 = HistoryModel()
        data8.date = "19 September 2019"
        data8.status = "1"
        dataList.add(data8)

        val data9 = HistoryModel()
        data9.date = "20 September 2019"
        data9.status = "1"
        dataList.add(data9)

        val data10 = HistoryModel()
        data10.date = "21 September 2019"
        data10.status = "1"
        dataList.add(data10)

        historyAdapter.notifyDataSetChanged()
    }
}
