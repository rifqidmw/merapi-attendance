package com.dev.merapitech.merapiattendance.utils

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log


class SQLiteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

//    val getChatHistory: ArrayList<HashMap<String, String>>
//        get() {
//            val wordList: ArrayList<HashMap<String, String>>
//            wordList = ArrayList()
//            val selectQuery = "SELECT * FROM $CHAT_HISTORY"
//            val database = this.writableDatabase
//            val cursor = database.rawQuery(selectQuery, null)
//            if (cursor.moveToFirst()) {
//                do {
//                    val map = HashMap<String, String>()
//                    map[COLUMN_ID] = cursor.getString(0)
//                    map[COLUMN_ID_MEDICINE] = cursor.getString(1)
//                    map[COLUMN_NAME] = cursor.getString(2)
//                    map[COLUMN_DATE_EXPIRED] = cursor.getString(3)
//                    map[COLUMN_BARCODE] = cursor.getString(4)
//                    map[COLUMNT_QTY] = cursor.getString(5)
//                    wordList.add(map)
//                } while (cursor.moveToNext())
//            }
//
//            Log.e("select sqlite ", "" + wordList)
//
//            database.close()
//            return wordList
//        }

    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_CHAT_HISTORY = "CREATE TABLE " + CHAT_HISTORY + " (" +
                CHAT_ID + " INTEGER PRIMARY KEY autoincrement, " +
                USERNAME + " TEXT NOT NULL, " +
                USERNAME_FRIEND + " TEXT NOT NULL, " +
                MESSAGE + " TEXT NOT NULL, " +
                ISMINE + " TEXT NOT NULL" +
                " )"

        db.execSQL(CREATE_CHAT_HISTORY)

//        val CREATE_TABLE_CART = "CREATE TABLE " + TABLE_CART + " (" +
//                COLUMN_ID + " INTEGER PRIMARY KEY autoincrement, " +
//                COLUMN_ID_CART + " TEXT NOT NULL, " +
//                COLUMN_ID_MEDICINE + " TEXT NOT NULL, " +
//                COLUMNT_QTY + " TEXT NOT NULL" +
//                " )"
//
//        db.execSQL(CREATE_TABLE_CART)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $CHAT_HISTORY")
        onCreate(db)
    }

    fun addChat(username: String, username_friend: String, message: String, is_mine: String) {
        val database = this.writableDatabase
        val queryValues = "INSERT INTO " + CHAT_HISTORY + " (username, username_friend, message, ismine) " +
                "VALUES ('" + username + "', '" + username_friend + "', '" + message+ "', '" + is_mine+ "')"

        Log.e("insert sqlite ", "" + queryValues)
        database.execSQL(queryValues)
        database.close()
    }

//    fun chatHistory(username: String, username_friend: String): MutableList<ChatModel> {
//        val chatList: MutableList<ChatModel>
//        chatList = ArrayList<ChatModel>()
//        val selectQuery = "SELECT * FROM $CHAT_HISTORY WHERE $USERNAME = '$username' AND $USERNAME_FRIEND = '$username_friend' OR $USERNAME = '$username_friend' AND $USERNAME_FRIEND = '$username'"
//        val database = this.writableDatabase
//        val cursor = database.rawQuery(selectQuery, null)
//        if (cursor.moveToFirst()) {
//            do {
//                val chatModel = ChatModel()
//                chatModel.senderName = cursor.getString(1)
//                chatModel.receiver = cursor.getString(2)
//                chatModel.body = cursor.getString(3)
//                chatModel.isMine = cursor.getString(4)
//                chatList.add(chatModel)
//            } while (cursor.moveToNext())
//        }
//
//        Log.e("select sqlite ", "" + chatList)
//
//        database.close()
//        return chatList
//    }

//    fun insertToCart(id_cart: String, id_medicine: String, qty: String) {
//        val database = this.writableDatabase
//        val queryValues = "INSERT INTO " + TABLE_MEDICINE + " (id_cart, id_medicine, qty) " +
//                "VALUES ('" + id_cart + "', '" + id_medicine + "', '" + qty + "')"
//
//        Log.e("insert sqlite ", "" + queryValues)
//        database.execSQL(queryValues)
//        database.close()
//    }

//    fun updateCart(id: Int, id_cart: String, id_medicine: String, qty: String) {
//        val database = this.writableDatabase
//
//        val updateQuery = ("UPDATE " + TABLE_MEDICINE + " SET "
//                + COLUMN_ID_CART + "='" + id_cart + "', "
//                + COLUMN_ID_MEDICINE + "='" + id_medicine + "', "
//                + COLUMNT_QTY + "='" + qty + "'"
//                + " WHERE " + COLUMN_ID + "=" + "'" + id + "'")
//        Log.e("update sqlite ", updateQuery)
//        database.execSQL(updateQuery)
//        database.close()
//    }
//
//    fun deleteFromCart(id: Int) {
//        val database = this.writableDatabase
//
//        val updateQuery = "DELETE FROM $TABLE_CART WHERE $COLUMN_ID='$id'"
//        Log.e("update sqlite ", updateQuery)
//        database.execSQL(updateQuery)
//        database.close()
//    }

    companion object {
        private val DATABASE_VERSION = 2

        internal val DATABASE_NAME = "monchat.db"
        val CHAT_HISTORY = "chat_history"
        val CHAT_ID = "id"
        val USERNAME = "username"
        val USERNAME_FRIEND = "username_friend"
        val MESSAGE = "message"
        val ISMINE = "ismine"
    }
}
