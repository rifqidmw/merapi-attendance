package com.dev.merapitech.merapiattendance.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.dev.merapitech.merapiattendance.R
import com.dev.merapitech.merapiattendance.TaskDetailActivity
import com.dev.merapitech.merapiattendance.model.HistoryModel
import com.dev.merapitech.merapiattendance.model.TaskModel

class TaskFinishAdapter(val context: Context, var list: ArrayList<TaskModel> = arrayListOf()) : RecyclerView.Adapter<TaskFinishAdapter.HistoryHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return HistoryHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
        val data = list[position]

        holder.tvDate.text = data.date
        holder.tvTitle.text = data.title
        holder.tvProject.text = data.project_name

        Log.e("DATE123", data.title)
    }

    inner class HistoryHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvDate: TextView = itemView.findViewById(R.id.tv_date)
        var tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        var tvProject: TextView = itemView.findViewById(R.id.tv_project)

    }
}

