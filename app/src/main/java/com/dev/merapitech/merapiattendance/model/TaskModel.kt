package com.dev.merapitech.merapiattendance.model

class TaskModel {
    lateinit var id: String
    lateinit var title: String
    lateinit var id_epic: String
    lateinit var epic_name: String
    lateinit var project_name: String
    lateinit var date: String
}