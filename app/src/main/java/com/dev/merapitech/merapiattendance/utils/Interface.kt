package com.dev.merapitech.merapiattendance.utils

import com.dev.merapitech.merapiattendance.model.request.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by XGibar on 27/10/2016.
 */
interface Interface {

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestLogin(@Body requestLogin: RequestLogin): Call<ResponseBody>

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestCheckApikey(@Body requestCheckApikey: RequestCheckApikey): Call<ResponseBody>

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestStatusAbsen(@Body requestStatusAbsen: RequestStatusAbsen): Call<ResponseBody>

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestAbsen(@Body requestAbsen: RequestAbsen): Call<ResponseBody>

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestHistory(@Body requestHistory: RequestHistory): Call<ResponseBody>

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestTask(@Body requestTask: RequestTask): Call<ResponseBody>

    @POST("http://172.27.13.231:7005/api/v1")
    fun requestFinishTask(@Body requestFinishTask: RequestFinishTask): Call<ResponseBody>
//    @Multipart
//    @POST("http://172.27.13.29/monchat/login.php")
//    fun requestLoginSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/register.php")
//    fun requestRegisterSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/updatename.php")
//    fun requestUpdateNameSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/updateemail.php")
//    fun requestUpdateEmailSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/acceptfriend.php")
//    fun requestUpdateRequestFriendSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/getfriend.php")
//    fun requestFriendListSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/invite.php")
//    fun requestInviteFriendSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
//
//    @Multipart
//    @POST("http://172.27.13.29/monchat/requestfriend.php")
//    fun requestInviteFriendListSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
}


