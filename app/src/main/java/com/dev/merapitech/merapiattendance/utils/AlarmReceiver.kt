package com.dev.merapitech.merapiattendance.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, p1: Intent?) {
        val service = Intent(context, NotificationService::class.java)
        service.putExtra("reason", service.getStringExtra("reason"))
        service.putExtra("timestamp", service.getLongExtra("timestamp", 0))

        context.startService(service)
    }
}