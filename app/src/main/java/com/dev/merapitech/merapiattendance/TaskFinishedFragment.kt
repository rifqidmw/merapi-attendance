package com.dev.merapitech.merapiattendance


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev.merapitech.merapiattendance.adapter.TaskAdapter
import com.dev.merapitech.merapiattendance.adapter.TaskFinishAdapter
import com.dev.merapitech.merapiattendance.model.TaskModel
import com.dev.merapitech.merapiattendance.utils.Api
import com.dev.merapitech.merapiattendance.utils.Handle
import com.dev.merapitech.merapiattendance.utils.ParamReq
import com.dev.merapitech.merapiattendance.utils.Session
import dmax.dialog.SpotsDialog
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TaskFinishedFragment : Fragment() {

    private lateinit var taskAdapter: TaskFinishAdapter
    private lateinit var dataList: ArrayList<TaskModel>
    private lateinit var session: Session
    private lateinit var dialog: android.app.AlertDialog
    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var rvTask: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_task_finished, container, false)

        dialog = SpotsDialog.Builder().setContext(context).build()
        session = Session(context!!)

        rvTask = view.findViewById(R.id.rv_task)

        Api.taskListFinish = ArrayList<TaskModel>()
        Api.taskListFinish.clear()
        taskAdapter = TaskFinishAdapter(context!!, Api.taskListFinish)

        rvTask.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvTask.itemAnimator = DefaultItemAnimator()
        rvTask.adapter = taskAdapter

        getTask()

        return view
    }

    private fun getTask(){
        dialog.show()
        val call = ParamReq.req2005("2005", session.get("apikey")!!, context!!)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleTaskFinish(response.body()!!.string(), context!!)
                    if (handle) {
                        taskAdapter.notifyDataSetChanged()
                        dialog.dismiss()
                    } else {
                        dialog.dismiss()
                        Toast.makeText(context!!, "Gagal Mengambil Task List", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(context!!, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(context!!, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(context!!, call, cBack, false, "Loading")
    }
    override fun onResume() {
        super.onResume()
        getTask()
    }

}
