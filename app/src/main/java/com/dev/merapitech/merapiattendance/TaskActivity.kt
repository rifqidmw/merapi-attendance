package com.dev.merapitech.merapiattendance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_task.*

class TaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        view_pager.adapter = PagerAdapter(supportFragmentManager, tabLayout.tabCount)
        view_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setupWithViewPager(view_pager)
        tabLayout.getTabAt(0)!!.text = "Ongoing"
        tabLayout.getTabAt(1)!!.text = "Finished"
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@TaskActivity.finish()
        }
    }

    inner class PagerAdapter(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentStatePagerAdapter(fm) {


        override fun getItem(position: Int): Fragment {

            when (position) {
                0 -> return TaskOngoingFragment()
                1 -> return TaskFinishedFragment()

                else -> return null!!
            }
        }

        override fun getCount(): Int {
            return mNumOfTabs
        }
    }
}
