package com.dev.merapitech.merapiattendance

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.budiyev.android.codescanner.*
import com.dev.merapitech.merapiattendance.utils.Api
import com.dev.merapitech.merapiattendance.utils.Handle
import com.dev.merapitech.merapiattendance.utils.ParamReq
import com.dev.merapitech.merapiattendance.utils.Session
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_check.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

class CheckActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session
    private lateinit var dialog: android.app.AlertDialog
    private lateinit var codeScanner: CodeScanner
    private lateinit var getIntent: Intent

    private var statusAbsen = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check)

        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)

        codeScanner = CodeScanner(this@CheckActivity, scannerView)
        session = Session(this@CheckActivity)
        dialog = SpotsDialog.Builder().setContext(this@CheckActivity).build()
        getIntent = intent

        statusAbsen = getIntent.getStringExtra("STATUS_ABSEN")!!

        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS
        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false

        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                absen(it.text)
            }
        }
        codeScanner.errorCallback = ErrorCallback {
            runOnUiThread {
                Toast.makeText(this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG).show()
            }
        }
        codeScanner.startPreview()
    }

    private fun absen(code: String){
        dialog.show()
        val call = ParamReq.req2002("2002", session.get("apikey")!!, statusAbsen, "2", code, this@CheckActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleAbsen(response.body()!!.string(), this@CheckActivity)
                    if (handle) {
                        dialog.dismiss()
                        showDialog()
                    } else {
                        dialog.dismiss()
                        codeScanner.startPreview()
                        Toast.makeText(this@CheckActivity, "QR sudah tidak berlaku, silahkan ulangi lagi", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@CheckActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                dialog.dismiss()
                Api.retryDialog(this@CheckActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@CheckActivity, call, cBack, false, "Loading")
    }

    private fun showDialog(){
        val dialog1 = Dialog(this@CheckActivity)
        dialog1.setContentView(R.layout.dialog_absen)
        dialog1.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog1.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val tvHeader: TextView
        val tvBody: TextView

        tvHeader = dialog1.findViewById(R.id.tv_absen_header)
        tvBody = dialog1.findViewById(R.id.tv_absen_body)

        if (statusAbsen == "1"){
            tvHeader.text = "Selamat, Check In Berhasil Dilakukan"
            tvBody.text = "Selamat Bekerja"
        } else {
            tvHeader.text = "Selamat, Check Out Berhasil Dilakukan"
            tvBody.text = "Hati-Hati Di Jalan Pulangnya"
        }

        val handler = Handler()
        handler.postDelayed({
            startActivity(Intent(this@CheckActivity, MainActivity::class.java))
            finish()
        }, 3000L)

        dialog1.show()
    }
}
