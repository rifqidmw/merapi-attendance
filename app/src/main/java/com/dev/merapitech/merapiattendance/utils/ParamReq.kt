package com.dev.merapitech.merapiattendance.utils

import android.annotation.SuppressLint
import android.content.Context
import com.dev.merapitech.merapiattendance.MainActivity
import com.dev.merapitech.merapiattendance.R
import com.dev.merapitech.merapiattendance.model.TaskModel
import com.dev.merapitech.merapiattendance.model.request.*
import okhttp3.ResponseBody
import retrofit2.Call

@SuppressLint("StaticFieldLeak")
object ParamReq {

    var context: Context? = null
    private var APIInterface: Interface? = null

    fun merapiAbsensi(): MainActivity {
        return context as MainActivity
    }

    fun req1001(hCode: String, email: String, password: String, id_device: String, jenis_device: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val data = RequestLogin.DataLogin()
        data.email = email
        data.password = password
        data.id_device = id_device
        data.jenis_device = jenis_device
        val params = RequestLogin()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.data = data
        return APIInterface!!.requestLogin(params)
    }

    fun req1002(hCode: String, token: String, id_device: String, jenis_device: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val data = RequestCheckApikey.DataCheckApiKey()
        data.token = token
        data.id_device = id_device
        data.jenis_device = jenis_device
        val params = RequestCheckApikey()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.data = data
        return APIInterface!!.requestCheckApikey(params)
    }

    fun req1008(hCode: String, apikey: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val params = RequestStatusAbsen()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.apikey = apikey
        return APIInterface!!.requestStatusAbsen(params)
    }

    fun req2002(hCode: String, apikey: String, status: String, type: String, code: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val data = RequestAbsen.DataAbsen()
        data.status = status
        data.type = type
        data.code = code
        val params = RequestAbsen()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.apikey = apikey
        params.data = data
        return APIInterface!!.requestAbsen(params)
    }

    fun req2003(hCode: String, apikey: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val params = RequestHistory()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.apikey = apikey
        return APIInterface!!.requestHistory(params)
    }

    fun req2004(hCode: String, apikey: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val params = RequestTask()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.apikey = apikey
        return APIInterface!!.requestTask(params)
    }

    fun req2005(hCode: String, apikey: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val params = RequestTask()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.apikey = apikey
        return APIInterface!!.requestTask(params)
    }

    fun req2006(hCode: String, apikey: String, id: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val data = RequestFinishTask.DataFinishTask()
        data.id = id
        val params = RequestFinishTask()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.apikey = apikey
        params.data = data
        return APIInterface!!.requestFinishTask(params)
    }

}
