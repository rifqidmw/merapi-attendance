package com.dev.merapitech.merapiattendance.model.request

class RequestAbsen {
    lateinit var HeaderCode: String
    lateinit var signature: String
    lateinit var apikey: String
    lateinit var data: DataAbsen

    class DataAbsen{
        lateinit var status: String
        lateinit var type: String
        lateinit var code: String
    }
}