package com.dev.merapitech.merapiattendance.model.request

class RequestFinishTask {
    lateinit var HeaderCode: String
    lateinit var signature: String
    lateinit var apikey: String
    lateinit var data: DataFinishTask
    class DataFinishTask{
        lateinit var id: String
    }
}