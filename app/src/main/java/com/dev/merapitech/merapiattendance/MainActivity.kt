package com.dev.merapitech.merapiattendance

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.merapitech.merapiattendance.adapter.HistoryAdapter
import com.dev.merapitech.merapiattendance.model.HistoryModel
import com.dev.merapitech.merapiattendance.utils.Api
import com.dev.merapitech.merapiattendance.utils.Handle
import com.dev.merapitech.merapiattendance.utils.ParamReq
import com.dev.merapitech.merapiattendance.utils.Session
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

class MainActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var dataList: ArrayList<HistoryModel>
    private lateinit var session: Session
    private lateinit var dialog: android.app.AlertDialog
    private var imeiDevice: String = ""
    private var token: String = ""

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        session = Session(this@MainActivity)
        dialog = SpotsDialog.Builder().setContext(this@MainActivity).build()

        token = session.get("apikey")!!

        Api.historyList = ArrayList<HistoryModel>()
        historyAdapter = HistoryAdapter(this, Api.historyList, 1)

        rv_history.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_history.itemAnimator = DefaultItemAnimator()
        rv_history.adapter = historyAdapter

        Glide.with(this@MainActivity).load(R.drawable.image).into(img_profile)

        btn_check_in.setOnClickListener {
            onCameraCheckPermission("1")
        }

        btn_check_out.setOnClickListener {
            onCameraCheckPermission("4")
        }

        btn_task.setOnClickListener {
            startActivity(Intent(this@MainActivity, TaskActivity::class.java))
        }

        btn_load_more.setOnClickListener {
            val activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this@MainActivity, rv_history, "recyclerHistory")

            val intent = Intent(this@MainActivity, HistoryActivity::class.java)
            startActivity(intent, activityOptionsCompat.toBundle())
        }

        tv_nama.text = session.get("full_name")
        tv_unit_name.text = session.get("unit_name")

        onImeiCheckPermission()
        checkApikey()
        getHistory()
        statusAbsen()

    }

    private fun checkApikey(){
        dialog.show()
        val call = ParamReq.req1002("1002", token, imeiDevice, "1", this@MainActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleCheckApiKey(response.body()!!.string(), this@MainActivity)
                    if (handle) {
                        dialog.dismiss()
                    } else {
                        dialog.dismiss()
                        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@MainActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@MainActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@MainActivity, call, cBack, false, "Loading")
    }

    private fun statusAbsen(){
        dialog.show()
        val call = ParamReq.req1008("1008", session.get("apikey")!!, this@MainActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {

                    val handle = Handle.handleStatusAbsen(response.body()!!.string(), tv_check_in, tv_check_out, btn_check_in, btn_check_out, this@MainActivity)
                    if (handle) {
                        dialog.dismiss()
                    } else {
                        dialog.dismiss()
                        Toast.makeText(this@MainActivity, "Gagal Mengambil Status Absen", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@MainActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@MainActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@MainActivity, call, cBack, false, "Loading")
    }

    private fun getHistory(){
        dialog.show()
        val call = ParamReq.req2003("2003", session.get("apikey")!!, this@MainActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleHistory(response.body()!!.string(), this@MainActivity)
                    if (handle) {
                        historyAdapter.notifyDataSetChanged()
                        dialog.dismiss()
                    } else {
                        dialog.dismiss()
                        Toast.makeText(this@MainActivity, "Gagal Mengambil History Absen", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@MainActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@MainActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@MainActivity, call, cBack, false, "Loading")
    }

    private fun onImeiCheckPermission(){
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.READ_PHONE_STATE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        val tel = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

                        if (ActivityCompat.checkSelfPermission(this@MainActivity,
                                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        }
                        val imei = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            tel.imei
                        } else {
                            tel.deviceId
                        }

                        imeiDevice = imei
                        if (imei != ""){
                        }else {
                            Toast.makeText(this@MainActivity, "Gagal mengambil IMEI Device", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        showSettingsDialog()
                    }

                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }


    private fun onCameraCheckPermission(status: String){
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        val intent = Intent(this@MainActivity, CheckActivity::class.java)
                        intent.putExtra("STATUS_ABSEN", status)
                        startActivity(intent)
                    } else {
                        showSettingsDialog()
                    }

                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()

    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

}
