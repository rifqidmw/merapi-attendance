package com.dev.merapitech.merapiattendance.utils

import java.text.SimpleDateFormat
import java.util.*

object DateTime {
    private val dateFormat = SimpleDateFormat("d MMM yyyy")
    private val timeFormat = SimpleDateFormat("K:mma")

    val currentTime: String
        get() {

            val today = Calendar.getInstance().time
            return timeFormat.format(today)
        }

    val currentDate: String
        get() {

            val today = Calendar.getInstance().time
            return dateFormat.format(today)
        }

}
