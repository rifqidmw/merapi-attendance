package com.dev.merapitech.merapiattendance.model.request

class RequestCheckApikey {
    lateinit var HeaderCode: String
    lateinit var signature: String
    lateinit var data: DataCheckApiKey
    class DataCheckApiKey{
        lateinit var token: String
        lateinit var id_device: String
        lateinit var jenis_device: String
    }
}