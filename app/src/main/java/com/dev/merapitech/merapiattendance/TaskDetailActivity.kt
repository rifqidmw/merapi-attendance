package com.dev.merapitech.merapiattendance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.dev.merapitech.merapiattendance.utils.Api
import com.dev.merapitech.merapiattendance.utils.Handle
import com.dev.merapitech.merapiattendance.utils.ParamReq
import com.dev.merapitech.merapiattendance.utils.Session
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_task_detail.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class TaskDetailActivity : AppCompatActivity() {

    private lateinit var dialog: android.app.AlertDialog
    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_detail)

        dialog = SpotsDialog.Builder().setContext(this@TaskDetailActivity).build()
        session = Session(this@TaskDetailActivity)
        val getIntent = intent

        tv_title.text = getIntent.getStringExtra("TITLE")
        tv_date.text = getIntent.getStringExtra("DATE")
        tv_project.text = getIntent.getStringExtra("PROJECT_NAME")

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@TaskDetailActivity.finish()
        }

        btn_proses.setOnClickListener {
            reqTaskFinish(getIntent.getStringExtra("ID"))
        }
    }

    private fun reqTaskFinish(id: String){
        dialog.show()
        val call = ParamReq.req2006("2006", session.get("apikey")!!, id, this@TaskDetailActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        onBackPressed()
                        this@TaskDetailActivity.finish()
                        Toast.makeText(this@TaskDetailActivity, "Task berhasil diproses finish", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this@TaskDetailActivity, "Gagal memproses Task finish", Toast.LENGTH_LONG).show()
                    }

                } catch (e: Exception) {
                    dialog.dismiss()
                    Toast.makeText(this@TaskDetailActivity, "Tidak dapat terhubung ke Server", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@TaskDetailActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@TaskDetailActivity, call, cBack, false, "Loading")
    }
}
